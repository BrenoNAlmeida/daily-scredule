from django.urls import path

from . import views

app_name='cronograma'
urlpatterns = [
    path('', views.index, name='index'),
    path('dailyschedule/<int:pk>/', views.detalhes.as_view(), name='detalhes'),
    path('dailyschedule/cadastrar/', views.gerarCronograma, name='gerarCronograma'),
    path('dailyschedule/criar-cronograma/', views.criarCronograma, name='criarCronograma'),
    path('dailyschedule/info/', views.criarInfo.as_view(), name='criarInfo'),
    path('dailyschedule/aulas/', views.criarAulas.as_view(), name='criarAulas'),
    path('dailyschedule/materias/', views.criarMaterias.as_view(), name='criarMaterias'),
    path('dailyschedule/provas/', views.criarProvas.as_view(), name='criarProvas'),
    path('dailyschedule/afazer/', views.criarAfazer.as_view(), name='criarAfazer'),
    path('dailyschedule/horario-vago/', views.criarHVago.as_view(), name='criarHVago'),


    path('dailyschedule/entrar/', views.Login.as_view(), name='Login'),
    path('dailyschedule/cadastro/', views.Cadastrar.as_view(), name='Cadastrar'),
    path('dailyschedule/logout/', views.Logout, name='Logout'),

    path('dailyschedule/meus-cronogramas/', views.meusCronogramas.as_view(), name='meusCronogramas'),

    path('dailyschedule/editar/<int:pk>/', views.editarCronograma.as_view(), name='editarCronograma'),
    path('dailyschedule/HabEditar/<int:pk>/', views.HabEditar.as_view(), name='HabEditar'),
    path('dailyschedule/excluirCronograma/<int:pk>/', views.excluirCronograma.as_view(), name='excluirCronograma'),

]