from django.db import models

from django.db import models
from django.utils import timezone
import datetime

class Aluno(models.Model):
    nome = models.CharField(max_length=30)
    usuario = models.CharField(max_length=30)
    senha = models.CharField(max_length=30)

    def __str__(self):
        return self.usuario

class Cronograma(models.Model):
    privacidade = models.BooleanField(default = False)
    titulo = models.CharField(max_length=100)
    aluno = models.ForeignKey(Aluno, on_delete=models.CASCADE)

    def __str__(self):
        return ("{0} - {1}").format(self.titulo, self.aluno)

class Tarefa(models.Model):
    titulo = models.CharField(max_length=50)
    assunto = models.CharField(max_length=50, null=True)
    descricao = models.CharField(max_length=100)
    hora_inicio = models.TimeField(default=timezone.now)
    hora_fim = models.TimeField(null=True)
    data = models.DateTimeField('Data Cronograma')
    status = models.BooleanField(default=False)
    cronograma = models.ForeignKey(Cronograma, on_delete=models.CASCADE)

    def desta_semana(self):
        now = datetime.date.today()
        week= now.isocalendar().week()
        week_trf= self.data.isocalendar().week()
        return (week_trf == week)

    def __str__(self):
        return self.descricao

class Tipo(models.Model):
    nome = models.CharField(max_length=12)
    tarefa = models.ForeignKey(Tarefa, on_delete=models.CASCADE)

    def __str__(self):
        return self.nome





