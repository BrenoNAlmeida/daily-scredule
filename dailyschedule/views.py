import datetime
#from django.utils import timezone
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import View

from .models import Cronograma, Aluno, Tarefa


def index(request):
    search = request.GET.get('search')
    if search:
        return busca(request)
    return render(request, 'dailyschedule/index.html')

def busca(request):
    latest_ds_list = Cronograma.objects.all()
    search = request.GET.get('search')
    if search:
        latest_ds_list = latest_ds_list.filter(titulo__icontains=search)
    context = {'latest_ds_list': latest_ds_list}
    return render(request, 'dailyschedule/busca.html', context)

class detalhes(View):
    def get(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=kwargs['pk'])
        tarefas = self.tarefas_da_semana(cronograma)
        contexto = {'cronograma':cronograma, 'tarefas':tarefas}

        return render(request, 'dailyschedule/detalhes.html',contexto)

    def tarefas_da_semana(self, cronograma):
        tarefas = Tarefa.objects.all()
        inicio = self.getInicio()
        fim = inicio + datetime.timedelta(days=7)
        ##return Tarefa.objects.all().order_by('data')
        #return Tarefa.objects.filter(data__week_day__gt=inicio).filter(data__week_day__lte=fim).order_by('data')
        return Tarefa.objects.filter(cronograma=cronograma).filter(data__gt=inicio).filter(data__lte=fim).order_by('data')
        #return Tarefa.objects.filter(cronograma=cronograma).filter(data_isocalendar()[1]==inicio_isocalendar()[1]).filter(data_isocalendar()[1]==fim_isocalendar()).order_by('data')

    def getInicio(self):
        now = datetime.date.today()
        inicio = now
        if now.weekday == 1:
            inicio = now - datetime.timedelta(days=1)
        elif now.weekday == 2:
            inicio = now - datetime.timedelta(days=2)
        elif now.weekday == 3:
            inicio = now - datetime.timedelta(days=3)
        elif now.weekday == 4:
            inicio = now - datetime.timedelta(days=4)
        elif now.weekday == 5:
            inicio = now - datetime.timedelta(days=5)
        elif now.weekday == 6:
            inicio = now - datetime.timedelta(days=6)
        return inicio

    def getFim(self):
        now = datetime.date.today()
        fim = now
        if now.weekday == 0:
            fim = now + datetime.timedelta(days=6)
        elif now.weekday == 1:
            fim = now + datetime.timedelta(days=5)
        elif now.weekday == 2:
            fim = now + datetime.timedelta(days=4)
        elif now.weekday == 3:
            fim = now + datetime.timedelta(days=3)
        elif now.weekday == 4:
            fim = now + datetime.timedelta(days=2)
        elif now.weekday == 5:
            fim = now + datetime.timedelta(days=1)
        return fim


def gerarCronograma(request):
    return render(request, 'dailyschedule/gerarCronograma.html')

def criarCronograma(request):
    try:
        if request.session['id_user']:
            search = request.GET.get('search')
            if search:
                return busca(request)
            return render(request, 'dailyschedule/criar-cronograma.html')
    except:
        return render(request, 'dailyschedule/index.html')


class criarInfo(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/info.html')

    def post(self, request, *args, **kwargs):
        titulo_cronograma = request.POST['titulo_cronograma']
        aluno = get_object_or_404(Aluno, pk=request.session['id_user'])
        cronograma = Cronograma(titulo=titulo_cronograma, aluno = aluno)

        privacidade = request.POST.getlist('priv')
        for priv in privacidade:
            if priv=='1':
                cronograma.privacidade=True

            elif priv=='2':

                cronograma.privacidade=False
            else:
                erro = 'Dados inválidos!'
                return render(request, 'dailyschedule/criar-cronograma.html',{'msg-erro': erro})

        cronograma.save()
        request.session['cronograma'] = cronograma.id
        return render(request, 'dailyschedule/criar-cronograma.html')

class criarAulas(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/aulas.html')

    def post(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=request.session['cronograma'])
        i = 1
        name = 'titulo_aula_'
        try:
            while request.POST[f'{name}{i}']:
                aula = Tarefa(titulo=request.POST['titulo_aula_'+str(i)],
                descricao = request.POST['descricao_aula_'+str(i)], hora_inicio=request.POST['horario_inicio_aula_'+str(i)],
                hora_fim=request.POST['horario_fim_aula_'+str(i)], data=request.POST['data_aula_'+str(i)], cronograma = cronograma)
                aula.save()
                i=i+1
        except:
            return render(request, 'dailyschedule/criar-cronograma.html')

        return render(request, 'dailyschedule/criar-cronograma.html')

class criarMaterias(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/materias.html')

    def post(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=request.session['cronograma'])
        i = 1
        name = 'titulo_materia_'
        try:
            while request.POST[f'{name}{i}']:
                materia = Tarefa(titulo=request.POST['titulo_materia_'+str(i)], assunto=request.POST['assunto_'+str(i)],
                descricao = request.POST['descricao_materia_'+str(i)], hora_inicio=request.POST['horario_inicio_materia_'+str(i)],
                hora_fim=request.POST['horario_fim_materia_'+str(i)], data=request.POST['data_materia_'+str(i)], cronograma = cronograma)
                materia.save()
                i=i+1
        except:
            return render(request, 'dailyschedule/criar-cronograma.html')       
        return render(request, 'dailyschedule/criar-cronograma.html')

class criarProvas(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/provas.html')

    def post(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=request.session['cronograma'])
        i = 1
        name = 'titulo_prova_'
        try:
            while request.POST[f'{name}{i}']:
                prova = Tarefa(titulo=request.POST['titulo_prova_'+str(i)], assunto=request.POST['assunto_'+str(i)],
                descricao = request.POST['descricao_prova_'+str(i)], hora_inicio=request.POST['horario_inicio_prova_'+str(i)],
                hora_fim=request.POST['horario_fim_prova_'+str(i)], data=request.POST['data_prova_'+str(i)], cronograma = cronograma)
                prova.save()
                i=i+1
        except:
            return render(request, 'dailyschedule/criar-cronograma.html')
        return render(request, 'dailyschedule/criar-cronograma.html')

class criarAfazer(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/afazeres.html')

    def post(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=request.session['cronograma'])
        i = 1
        name = 'titulo_afazer_'
        try:
            while request.POST[f'{name}{i}']:
                afazer = Tarefa(titulo=request.POST['titulo_afazer_'+str(i)],
                descricao = request.POST['descricao_afazer_'+str(i)], hora_inicio=request.POST['horario_inicio_afazer_'+str(i)],
                hora_fim=request.POST['horario_fim_afazer_'+str(i)], data=request.POST['data_afazer_'+str(i)], cronograma = cronograma)
                afazer.save()
                i=i+1
        except:
            return render(request, 'dailyschedule/criar-cronograma.html')
        return render(request, 'dailyschedule/criar-cronograma.html')

class criarHVago(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'dailyschedule/hvago.html')

    def post(self, request, *args, **kwargs):
        cronograma = get_object_or_404(Cronograma, pk=request.session['cronograma'])
        i = 1
        name = 'titulo_hvago_'
        del request.session['cronograma']
        tarefas = self.tarefas_da_semana(cronograma)
        contexto = {'cronograma':cronograma,'tarefas':tarefas}
        try:
            while request.POST[f'{name}{i}']:
                hvago = Tarefa(titulo=request.POST['titulo_hvago_'+str(i)],
                descricao = request.POST['descricao_hvago_'+str(i)], hora_inicio=request.POST['horario_inicio_hvago_'+str(i)],
                hora_fim=request.POST['horario_fim_hvago_'+str(i)], data=request.POST['data_hvago_'+str(i)], cronograma = cronograma)
                hvago.save()
                i=i+1
        except:
            return render(request, 'dailyschedule/detalhes.html',contexto)
        return render(request, 'dailyschedule/detalhes.html',contexto)

    def tarefas_da_semana(self, cronograma):
        tarefas = Tarefa.objects.all()
        inicio = self.getInicio()
        fim = inicio + datetime.timedelta(days=7)
        return Tarefa.objects.filter(cronograma=cronograma).filter(data__gt=inicio).filter(data__lte=fim).order_by('data')

    def getInicio(self):
        now = datetime.date.today()
        inicio = now
        if now.weekday == 1:
            inicio = now - datetime.timedelta(days=1)
        elif now.weekday == 2:
            inicio = now - datetime.timedelta(days=2)
        elif now.weekday == 3:
            inicio = now - datetime.timedelta(days=3)
        elif now.weekday == 4:
            inicio = now - datetime.timedelta(days=4)
        elif now.weekday == 5:
            inicio = now - datetime.timedelta(days=5)
        elif now.weekday == 6:
            inicio = now - datetime.timedelta(days=6)
        return inicio

class Login(View):
    def get(self, request, *args, **kwargs):
        search = request.GET.get('search')
        if search:
            return busca(request)
        return render(request, 'dailyschedule/login.html')
    def post(self, request, *args, **kwargs):
        aluno = get_object_or_404(Aluno, usuario=request.POST['username'])
        if aluno.senha != request.POST['password']:
            return render(request, 'dailyschedule/login.html')
        request.session['id_user'] = aluno.id
        return render(request, 'dailyschedule/criar-cronograma.html')

class Cadastrar(View):
    def get(self, request, *args, **kwargs):
        search = request.GET.get('search')
        if search:
            return busca(request)
        return render(request, 'dailyschedule/cadastro.html')
    def post(self, request, *args, **kwargs):
        usuario = Aluno(nome = request.POST['name'], usuario = request.POST['user'], senha = request.POST['password'])
        usuario.save()
        request.session['id_user'] = usuario.id
        return render(request, 'dailyschedule/criar-cronograma.html')

def Logout(request):
    del request.session['id_user']
    return render(request, 'dailyschedule/index.html')


class meusCronogramas(View):
    def get(self, request, *args, **kwargs):
        if request.session['id_user']:    
            aluno = get_object_or_404(Aluno, pk=request.session['id_user'])        
            search = request.GET.get('search')
            if search:
                return busca(request)
            latest_ds_list = Cronograma.objects.order_by('-titulo')
            context = {'latest_ds_list': latest_ds_list}
            crono_aluno = aluno.cronograma_set.all()
            return render(request, 'dailyschedule/meus-cronogramas.html', {'latest_ds_list':crono_aluno})
        return render(request, 'dailyschedule/index.html')

class editarCronograma(View):
    def get(self,request,*args,**kwargs):
        tarefa = get_object_or_404(Tarefa, pk=kwargs['pk'])
        return render(request, 'dailyschedule/editar.html',{'tarefa':tarefa})
    def post(self,request,*args,**kwargs):
        tarefa = get_object_or_404(Tarefa, pk=request.POST['id'])
        tarefa.titulo = request.POST['titulo']
        tarefa.descricao = request.POST['descricao']
        tarefa.horario_inicio = request.POST['horario_inicio']
        tarefa.horario_fim = request.POST['horario_fim']
        tarefa.data = request.POST['data']
        tarefa.save()
        return render(request, 'dailyschedule/detalhes.html',{'msg':'Tarefa editada com sucesso', 'cronograma':tarefa.cronograma})

class HabEditar(View):
    def get(self,request,*args,**kwargs):
        cronograma = get_object_or_404(Cronograma, pk=kwargs['pk'])
        tarefas = self.tarefas_da_semana(cronograma)
        contexto = {'cronograma':cronograma,'tarefas':tarefas}
        return render(request, 'dailyschedule/HabEditar.html',contexto)
    
    def post(self,request,*args,**kwargs):
        cronograma = get_object_or_404(Cronograma, pk=kwargs['pk'])
        privacidade = request.POST.getlist('priv')
        for priv in privacidade:
            if priv=='1':
                cronograma.privacidade=True

            elif priv=='2':

                cronograma.privacidade=False
            else:
                erro = 'Dados inválidos!'
                return render(request, 'dailyschedule/meusCronogramas.html',{'msg-erro': erro})
        tarefas = self.tarefas_da_semana(cronograma)
        contexto = {'cronograma':cronograma,'tarefas':tarefas}
        return render(request, 'dailyschedule/detalhes.html',contexto)
        cronograma.save()

    def tarefas_da_semana(self, cronograma):
        tarefas = Tarefa.objects.all()
        inicio = self.getInicio()
        fim = inicio + datetime.timedelta(days=7)
        return Tarefa.objects.filter(cronograma=cronograma).filter(data__gt=inicio).filter(data__lte=fim).order_by('data')

    def getInicio(self):
        now = datetime.date.today()
        inicio = now
        if now.weekday == 1:
            inicio = now - datetime.timedelta(days=1)
        elif now.weekday == 2:
            inicio = now - datetime.timedelta(days=2)
        elif now.weekday == 3:
            inicio = now - datetime.timedelta(days=3)
        elif now.weekday == 4:
            inicio = now - datetime.timedelta(days=4)
        elif now.weekday == 5:
            inicio = now - datetime.timedelta(days=5)
        elif now.weekday == 6:
            inicio = now - datetime.timedelta(days=6)
        return inicio


class excluirCronograma(View):
    def get(self,request,*args,**kwargs):
        cronograma = get_object_or_404(Cronograma, pk=kwargs['pk'])
        latest_ds_list = Cronograma.objects.all()
        cronograma.delete()
        return render(request, 'dailyschedule/meus-cronogramas.html',{'msg':'Cronograma removido com sucesso!','latest_ds_list':latest_ds_list})






