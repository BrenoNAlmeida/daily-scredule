var lineCount = 1;
addMatterLine = function () {
    var i = document.createElement('input');
    var x = document.createElement('span');
    i.setAttribute("type", "text");
    i.id = lineCount;
    i.setAttribute("placeholder", "Address Line " + ++lineCount);
    var addressContainer = document.getElementById("adress");

    //<div style="display:flex; gap:10px; margin-bottom:15px;">

    const parent = document.querySelector('#adress');

    const child = document.createElement('div');
    child.setAttribute('style','display:flex;  gap:10px; margin-bottom:10px;');
    child.setAttribute('class','info');

    parent.appendChild(child);




    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'titulo_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'titulo_materia_' + lineCount);
    elemento.setAttribute('id', 'titulo_materia_' + lineCount);

    child.appendChild(elemento);


    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'assunto_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'assunto_' + lineCount);
    elemento.setAttribute('id', 'assunto_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'descricao_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'descricao_materia_' + lineCount);
    elemento.setAttribute('id', 'descricao_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'horario_inicio_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_inicio_materia_' + lineCount);
    elemento.setAttribute('id', 'horario_inicio_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'horario_fim_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_fim_materia_' + lineCount);
    elemento.setAttribute('id', 'horario_fim_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'data_materia_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'date');
    elemento.setAttribute('name', 'data_materia_' + lineCount);
    elemento.setAttribute('id', 'data_materia_' + lineCount);

    child.appendChild(elemento);


}

addClassLine = function () {

    const parent = document.querySelector('#adress');

    const child = document.createElement('div');
    child.setAttribute('style','display:flex;  gap:10px; margin-bottom:10px;');
    

    parent.appendChild(child);


    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'titulo_aula_' + lineCount);
    elemento.setAttribute('id', 'titulo_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'descricao_aula_' + lineCount);
    elemento.setAttribute('id', 'descricao_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_inicio_aula_' + lineCount);
    elemento.setAttribute('id', 'horario_inicio_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_fim_aula_' + lineCount);
    elemento.setAttribute('id', 'horario_fim_aula_' + lineCount);

    child.appendChild(elemento);


    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'date');
    elemento.setAttribute('name', 'data_aula_' + lineCount);
    elemento.setAttribute('id', 'data_aula_' + lineCount);

    child.appendChild(elemento);

}

addTestLine = function () {

    const parent = document.querySelector('#adress');

    const child = document.createElement('div');
    child.setAttribute('style','display:flex;  gap:10px; margin-bottom:10px;')

    parent.appendChild(child);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'titulo_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'titulo_aula_' + lineCount);
    elemento.setAttribute('id', 'titulo_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'descricao_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'text');
    elemento.setAttribute('name', 'descricao_aula_' + lineCount);
    elemento.setAttribute('id', 'descricao_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'horario_inicio_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_inicio_aula_' + lineCount);
    elemento.setAttribute('id', 'horario_inicio_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'horario' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'time');
    elemento.setAttribute('name', 'horario_fim_aula_' + lineCount);
    elemento.setAttribute('id', 'horario_fim_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('label');
    elemento.setAttribute('for', 'data_aula_' + lineCount);

    child.appendChild(elemento);

    var elemento = document.createElement('input');
    elemento.setAttribute('type', 'date');
    elemento.setAttribute('name', 'data_aula_' + lineCount);
    elemento.setAttribute('id', 'data_aula_' + lineCount);

    child.appendChild(elemento);

}